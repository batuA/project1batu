#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <assert.h>
#include "debug.h"
#include "rtlib.h"
//#include "rtgrading.h"

#include "sircd.h"
#include "irc_proto.h"
//#include "irc_proto.c"
#include <errno.h>

//A shorthand to make things easier.
#define TRUE   1
#define FALSE  0

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Uncomment 93 - related to debugging.
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


int channel_count;
client client_list[MAX_CLIENTS+1];
channels channels_list[MAX_CHANNEL+1];




u_long curr_nodeID;
rt_config_file_t   curr_node_config_file;  /* The config_file  for this node */
rt_config_entry_t *curr_node_config_entry; /* The config_entry for this node */

void init_node(char *nodeID, char *config_file);
void irc_server();


void start_server(int port){

    int client_count = 0;
    int address_length, client_sockets[MAX_CLIENTS], activity, value_read;
    int max_socket_fd, current_socket_fd, new_socket_fd;

    //index to be used in loops
    int i;

    char *message = "Welcome to the IRC chat server\n";

    // initilize a predetermined sockadd_in struct.
    struct sockaddr_in socket_address;
    // !!! address = socket_address

    //set of socket descriptors
    fd_set file_descriptor_set;

    // make a character buffer to get the message.
    char buffer[MAX_MSG_LEN];

    // create file descriptors
    int root_socket_fd;

    // initilize a client socket for each client and set it to 0.
    for (i = 0; i < MAX_CLIENTS; i++) {
        client_sockets[i] = 0;
    }

    //RETURN VALUE is a descriptor.
    //On success, a file descriptor for  the  new  socket  is  returned. On error, -1 is returned, and errno is set appropriately
    //AF_INET = IPV4
    //SOCK_STREAM = Provides sequenced, reliable, two-way, connection-based byte streams.  An out-of-band data transmission mechanism may be supported.

    // Create the root socket to initilize the main connection.
    if( (root_socket_fd = socket(AF_INET , SOCK_STREAM , 0)) == -1) {
        perror("Couldnt open the root socket");
        exit(EXIT_FAILURE);
    }

    //configure the socket.
    // family mentioned above
    socket_address.sin_family = AF_INET;

    //INADDR_ANY - use IPV4
    socket_address.sin_addr.s_addr = INADDR_ANY;

    // add in the port
    // The htons() function converts the unsigned short integer hostshort from host byte order to network byte order.
    socket_address.sin_port = htons( port );

    /*
        When  a  socket  is  created  with socket(2), it exists in a name space
       (address family) but has no address assigned to it.  bind() assigns the
       address  specified  by  addr  to  the  socket  referred  to by the file
       descriptor sockfd.  addrlen  specifies  the  size,  in  bytes,  of  the
       address structure pointed to by addr.  Traditionally, this operation is
       called “assigning a name to a socket”.
    */

    // bind root_socket file descriptor to socket_address which was configured above.
    if (bind(root_socket_fd, (struct sockaddr *)&socket_address, sizeof(socket_address))<0) {
        perror("Binding root_socket to socket_addres failed.");
        exit(EXIT_FAILURE);
    }

    //Figure this out.
    int opt = TRUE;
    if( setsockopt(root_socket_fd, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 ){
        perror("Setsockopt failed");
        exit(EXIT_FAILURE);
    }

    printf("Listener on port %d \n", port);

    /*
        listen()  marks  the  socket referred to by sockfd as a passive socket,
        that is, as a socket that will be used to  accept  incoming  connection
        requests using accept(2).
    */

    //backlog is the second argument. It sets the amount of queries.
    //try to specify maximum of 10 pending connections for the master socket
    if (listen(root_socket_fd, 10) < 0){
        perror("Error in listening.");
        exit(EXIT_FAILURE);
    }

    //accept the incoming connection
    address_length = sizeof(socket_address);
    printf("Now accepting connections\n");

    while(TRUE){

        // FD_ZERO(&fdset)
        // Initializes the file descriptor set fdset to have zero bits for all file descriptors.
        FD_ZERO(&file_descriptor_set);

        //FD_SET(fd, &fdset)
        //Sets the bit for the file descriptor fd in the file descriptor set fdset.
        //add master socket to set
        FD_SET(root_socket_fd, &file_descriptor_set);
        max_socket_fd = root_socket_fd;

        //add the children sockets to the file descriptor set.
        for ( i = 0 ; i < MAX_CLIENTS ; i++) {
            // take the current file descriptor from the client_sockets list
            current_socket_fd = client_sockets[i];

            //if socket descriptor is not zero -is valid- add it to the set.
            if(current_socket_fd > 0)
                FD_SET( current_socket_fd , &file_descriptor_set);

            //Set the highest file descriptor.
            if(current_socket_fd > max_socket_fd)
                max_socket_fd = current_socket_fd;
        }

        /*
            int select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);

            nfds is the highest-numbered file descriptor in any of the three  sets,
            plus 1.

            select() and  pselect()  allow  a  program  to  monitor  multiple  file
            descriptors,  waiting  until one or more of the file descriptors become
            "ready" for some class of I/O operation (e.g., input possible).  A file
            descriptor  is  considered  ready if it is possible to perform a corre‐
            sponding I/O operation (e.g., read(2) without  blocking,  or  a  suffi‐
            ciently small write(2)).
        */

        // Select returns the available data. Select is polling routine.

        // select allows us to have multiple clients without forking or threading.
        activity = select( max_socket_fd + 1 , &file_descriptor_set , NULL , NULL , NULL);

        if ((activity < 0) && (errno != EINTR)){
            printf("Error at select command.");
        }

        //FD_ISSET(fd, &fdset)
        //Returns a non-zero value if the bit for the file descriptor fd is set in the file descriptor set pointed to by fdset, and 0 otherwise.

        // Check for incoming connections.
        if (FD_ISSET(root_socket_fd, &file_descriptor_set)) {


            if ((new_socket_fd = accept(root_socket_fd, (struct sockaddr *)&socket_address, (socklen_t*)&address_length))<0){
                perror("Error at accepting new socket in FD_ISSET.");
                exit(EXIT_FAILURE);
            }

            /*
                The inet_ntoa() function converts the Internet host address  in,  given
                in  network  byte  order,  to a string in IPv4 dotted-decimal notation.
                The string is returned in a statically allocated buffer,  which  subse‐
                quent calls will overwrite.
            */

            //inform user of socket number - used in send and receive commands
            printf("New connection , socket fd is %d , ip is : %s , port : %d \n" , new_socket_fd , inet_ntoa(socket_address.sin_addr) , ntohs(socket_address.sin_port));

            //increment the client count

            //initilize the list with 0s.
            client_list[client_count].sock = new_socket_fd;
            client_count++;

            // if the read is 0 it means the client has disconnet.



            /*
                ssize_t send(int sockfd, const void *buf, size_t len, int flags);

                The send() call may be used only when the  socket  is  in  a  connected
                state  (so  that the intended recipient is known).  The only difference
                between send() and write(2) is the presence  of  flags.   With  a  zero
                flags  argument, send() is equivalent to write(2).

                On success, these calls return the number of bytes sent.  On error,  -1
                is returned, and errno is set appropriately.
            */

            //send new connection greeting message to the IRC servers.
            // send message of strlen(message) length to the open socket in new_socket_fd
            if( send(new_socket_fd, message, strlen(message), 0) != strlen(message) ){
                perror("Error at sending the message.");
            }

            // add the new socket fd to the list of client sockets.
            for (i = 0; i < MAX_CLIENTS; i++){
                //iterate until empty space is found then plug it in.
                if( client_sockets[i] == 0 ){
                    client_sockets[i] = new_socket_fd;
                    printf("Adding to list of sockets as %d\n" , i);
                    break;
                }
            }
        } // FD_ISSET

        client active_client;
        //iterate throught the client sockets.
        for (i = 0; i < MAX_CLIENTS; i++) {

            // can be optimized here.
            current_socket_fd = client_sockets[i];
            if(client_list[i].sock == current_socket_fd){

                active_client = client_list[i];
            }


            current_socket_fd = client_sockets[i];

            /*
                ssize_t read(int fd, void *buf, size_t count);

                read()  attempts to read up to count bytes from file descriptor fd into
                the buffer starting at buf.

                On success, the number of bytes read is returned (zero indicates end of
                file), and the file position is advanced by this number.  It is not  an
                error  if  this  number  is smaller than the number of bytes requested;
                this may happen for example because fewer bytes are actually  available
                right  now  (maybe  because we were close to end-of-file, or because we
                are reading from a pipe, or from a terminal),  or  because  read()  was
                interrupted  by  a  signal.  On error, -1 is returned, and errno is set
                appropriately.  In this case, it is left unspecified whether  the  file
                position (if any) changes.
            */

            // check if the current socket fd is in the filedescriptor set.
            if (FD_ISSET( current_socket_fd , &file_descriptor_set)) {


                // Read what they send and pass it back!
                // Come
                value_read = read( current_socket_fd, buffer, MAX_MSG_LEN);
                buffer[value_read] = '\0';
                handle_line(buffer, current_socket_fd);
                //handle_line(buffer);
                //send(current_socket_fd , buffer , strlen(buffer) , 0 );
            }
        }

    } // While

}


void
usage() {
    fprintf(stderr, "sircd [-h] [-D debug_lvl] <nodeID> <config file>\n");
    exit(-1);
}



int main( int argc, char *argv[] )
{
    extern char *optarg;
    extern int optind;
    int ch;

    while ((ch = getopt(argc, argv, "hD:")) != -1)
        switch (ch) {
	case 'D':
	    //if (set_debug(optarg)) {
		//exit(0);
	    //}
	    break;
        case 'h':
        default: /* FALLTHROUGH */
            usage();
        }
    argc -= optind;
    argv += optind;

    if (argc < 2) {
	usage();
    }

    init_node(argv[0], argv[1]);


    int port = curr_node_config_entry->irc_port;
    //printf( "I am node %d and I listen on port %d for new users\n", curr_nodeID, (int)curr_node_config_entry->irc_port );

    /* Start your engines here! */

    start_server(port);


    return 0;
}

/*
 * void init_node( int argc, char *argv[] )
 *
 * Takes care of initializing a node for an IRC server
 * from the given command line arguments
 */
void
init_node(char *nodeID, char *config_file)
{
    int i;

    curr_nodeID = atol(nodeID);
    rt_parse_config_file("sircd", &curr_node_config_file, config_file );

    /* Get config file for this node */
    for( i = 0; i < curr_node_config_file.size; ++i )
        if( curr_node_config_file.entries[i].nodeID == curr_nodeID )
             curr_node_config_entry = &curr_node_config_file.entries[i];

    /* Check to see if nodeID is valid */
    if( !curr_node_config_entry )
    {
        printf( "Invalid NodeID\n" );
        exit(1);
    }
}

