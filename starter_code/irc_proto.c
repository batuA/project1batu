#include "irc_proto.h"
#include "debug.h"

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <fcntl.h>
#include "sircd.h"


#define MAX_COMMAND 16

/* You'll want to define the CMD_ARGS to match up with how you
 * keep track of clients.  Probably add a few args...
 * The command handler functions will look like
 * void cmd_nick(CMD_ARGS)
 * e.g., void cmd_nick(your_client_thingy *c, char *prefix, ...)
 * or however you set it up.
 */

//#define CMD_ARGS char *prefix, char **params, int n_params, int clientFD
#define CMD_ARGS char *prefix, char **params, int n_params, client client

//change the API pass the cleint.
// Second step get the buffer.

typedef void (*cmd_handler_t)(CMD_ARGS);
#define COMMAND(cmd_name) void cmd_name(CMD_ARGS)


struct dispatch {
    char cmd[MAX_COMMAND];
    int needreg; /* Must the user be registered to issue this cmd? */
    int minparams; /* send NEEDMOREPARAMS if < this many params */
    cmd_handler_t handler;
};



//client list moved to header
client client_list[MAX_CLIENTS + 1];
channels channels_list[MAX_CHANNEL + 1];
channel_count = 0;

#define NELMS(array) (sizeof(array) / sizeof(array[0]))

/* Define the command handlers here.  This is just a quick macro
 * to make it easy to set things up */
COMMAND(cmd_nick); //Predefined NICK
COMMAND(cmd_user); //Predefined USER
COMMAND(cmd_quit); //Predefined QUIT

//Channel Commands
COMMAND(cmd_part); // PART Command
COMMAND(cmd_list); // LIST Command
COMMAND(cmd_join); // JOIN Command

//Advanced Commands
COMMAND(cmd_privmsg);// Private Message Command
COMMAND(cmd_who);// WHO Command


// Defined in IRC HTML but not used in the assignment
/*
COMMAND(cmd_pass);   //PASSWORD Command
COMMAND(cmd_server); //SERVER Command
COMMAND(cmd_oper);   //OPER Command
COMMAND(cmd_squit);  //SQUIT Command
*/

/* Fill in the blanks */

/* Dispatch table.  "reg" means "user must be registered in order
 * to call this function".  "#param" is the # of parameters that
 * the command requires.  It may take more optional parameters.
 */
struct dispatch cmds[] = {
    /* cmd,    reg  #parm  function */
    { "NICK",    0, 1, cmd_nick },

    //
    { "USER",    0, 4, cmd_user },
    { "QUIT",    1, 0, cmd_quit },

    // The REG and PARM fields can change. Not sure as of now.

    // User has to be registered and it takes channel ID and departs specified channel.
    { "PART",    1, 1, cmd_part },
    // User has to be registered and takes no arguments and lists all the channels and user number.
    { "LIST",    0, 1, cmd_list },
    // User has to be registered and takes channel ID joins the user to that channel (limited to 1)
    { "JOIN",    1, 1, cmd_join },

    // User has to be registered and takes target name (user or channel)
    { "PRIVMSG", 1, 2, cmd_privmsg },
    // User has to be registered and takes target channel name and returns all users in that channel.
    { "WHO",     1, 1, cmd_who  },

    // For all other commands return ERR_UNKNOWNCOMMAND

    /* Fill in the blanks... */
};

/* Handle a command line.  NOTE:  You will probably want to
 * modify the way this function is called to pass in a client
 * pointer or a table pointer or something of that nature
 * so you know who to dispatch on...
 * Mostly, this is here to do the parsing and dispatching
 * for you
 *
 * This function takes a single line of text.  You MUST have
 * ensured that it's a complete line (i.e., don't just pass
 * it the result of calling read()).
 * Strip the trailing newline off before calling this function.
 */

 //in the server keep a client list of "HOST NAME" username. Link FD/HOSTNAME to username. NICK USER

 // HELPER FUNCTIONS DEFINED

 // Compare with disregarding the new line.
int special_compare (char* list_client, char* arg_client) {
  int leng = strlen(arg_client);
  int i;
  for(i = 0; i < leng; i++){
    if(strcasecmp(list_client[i], arg_client[i])){
        return 1;
    }
  }
  return 0;
}

// Removing the newline
void trim_string(char* string){
    char* newlineLocation = strstr(string, "\n");
    if (newlineLocation != NULL){
        strncpy(newlineLocation -1, "\0", 1);
    }
}

 // returns the client index from the list given a fd.
int clientIndex_from_list(int client_fd){

    int i;
    //iterate throught the client sockets.
    for (i = 0; i < MAX_CLIENTS; i++) {
        // can be optimized here.
        if(client_list[i].sock == client_fd){
            client_list[i].index = i;
            return i;
        }
    }
}

// returns clien FD given a nick.
int clientFD_from_nick(char* nick){

    int i;
    //iterate throught the client sockets.
    char* temp_nick;

    for (i = 0; i < MAX_CLIENTS; i++) {
        trim_string(client_list[i].nick);

        if(strcasecmp(client_list[i].nick, nick) == 0 ){
            return client_list[i].sock;
        }
    }
}


//Most of the messages sent to the server generate a reply of some sort. The most common reply is the numeric reply, used for both errors and normal replies. The numeric reply must be sent as one message consisting of the sender prefix, the three digit numeric, and the target of the reply. In all other respects, a numeric reply is just like a normal message, except that the keyword is made up of 3 numeric digits rather than a string of letters. A list of different replies is supplied in section 6.
//Example:
char* response_message_concat(int err, char* string, client client){

    char* response[MAX_MSG_LEN];
    sprintf(response, "%d", err);
    strcat(response, " ");
    strcat(response, string);
    strcat(response, " ");
    strcat(response, client.nick);
    strcat(response, "\n");

    return response;
}





void handle_line(char *line, int client_fd){

    char response[MAX_MSG_LEN];
    char *prefix = NULL, *command, *pstart, *params[MAX_MSG_TOKENS];
    int n_params = 0;
    char *trailing = NULL;

    int clientIndex = clientIndex_from_list(client_fd);
    client active_client = client_list[clientIndex];

    DPRINTF(DEBUG_INPUT, "Handling line:%d %s\n" ,client_fd, line);
    command = line;




    if (*line == ':') {
        prefix = ++line;
        command = strchr(prefix, ' ');
    }
    if (!command || *command == '\0') {

    }

    while (*command == ' ') {
        *command++ = 0;
    }
    if (*command == '\0') {
        //and_more_of_your_code_should_go_here();
        /* Send an unknown command error! */
        return;
    }
    pstart = strchr(command, ' ');
    if (pstart) {
        while (*pstart == ' ') {
            *pstart++ = '\0';
	}
	if (*pstart == ':') {
	    trailing = pstart;
	} else {
	    trailing = strstr(pstart, " :");
	}
	if (trailing) {
	    while (*trailing == ' ')
		*trailing++ = 0;
	    if (*trailing == ':')
		*trailing++ = 0;
	}
	do {
	    if (*pstart != '\0') {
		params[n_params++] = pstart;
	    } else {
		break;
	    }
	    pstart = strchr(pstart, ' ');
	    if (pstart) {
            while (*pstart == ' ') {
                *pstart++ = '\0';
            }
	    }
	} while (pstart != NULL && n_params < MAX_MSG_TOKENS);
    }

    if (trailing && n_params < MAX_MSG_TOKENS) {
	params[n_params++] = trailing;
    }

    DPRINTF(DEBUG_INPUT, "Prefix:  %s\nCommand: %s\nParams (%d):\n",
	    prefix ? prefix : "<none>", command, n_params);

    //printf("Prefix:  %s\nCommand: %s\nParams (%d):\n", prefix ? prefix : "<none>", command, n_params);

    int i;
    for (i = 0; i < n_params; i++) {
	DPRINTF(DEBUG_INPUT, "   %s\n", params[i]);
    }
    DPRINTF(DEBUG_INPUT, "\n");

    int index = clientIndex_from_list(client_fd);
    // THIS IS THE COMMAND ENGINE
    for (i = 0; i < NELMS(cmds); i++) {
        //printf("printing cmds |%s|\n", cmds[i].cmd);
        if (!strcasecmp(cmds[i].cmd, command)) {
            if (cmds[i].needreg && active_client.registered == 0 ) {
                send_response(response_message_concat(ERR_NOTREGISTERED, "You are not registed.", client_list[index]), client_list[index].sock);

            } else if (n_params < cmds[i].minparams) {

                send_response(response_message_concat(ERR_NEEDMOREPARAMS, "Not enough parameters given", client_list[index]), client_list[index].sock);
            } else {
                (*cmds[i].handler)(prefix, params, n_params, active_client);
                //(*cmds[i].handler)(CMD_ARGS);
            }
            break;
        }
    }
    if (i == NELMS(cmds)) {
        int j;
        /* ERROR - unknown command! */
        index = clientIndex_from_list(client_fd);
        send_response(response_message_concat(ERR_UNKNOWNCOMMAND, "Unknown command", client_list[index]), client_list[index].sock);
    }
}




// Checks whether the user count on a channel is 0, if so deletes the channel.
void delete_channel(int channel_index){
    if(channels_list[channel_index].client_count <= 0){
        channels_list[channel_index].client_count = 0;
        channel_count--;
        DPRINTF("Channel: %s is deleted and has %d users.\n", channels_list[channel_index].channel_name,
                                                             channels_list[channel_index].client_count);
        channels_list[channel_index] = channels_list[MAX_CHANNEL];
    } else{
        DPRINTF("Channel: %s is NOT deleted, still has %d users.\n", channels_list[channel_index].channel_name,
                                                                    channels_list[channel_index].client_count);
    }

}

// sends response to the active client.
void send_response(char* response, int active_client_fd){

    DPRINTF("Sending %s to %d\n", response, active_client_fd);
    send(active_client_fd , response , strlen(response) , 0 );
}

// sends the given response to every member in the channel
void send_channel(char* response, char* channel_name){
    int i,j;
    for (i = 0; i < MAX_CHANNEL; i++) {
        if(!strcasecmp(channels_list[i].channel_name, channel_name)){
            DPRINTF("I am right before the for loop\n");
            for(j = 0; j < MAX_CLIENTS; j++){
                //printf("Channel_lists[%d].users[%d] (cnl_nm: %s) sock is %d\n", i, j, channels_list[i].channel_name ,channels_list[i].users[j].sock);
                if(channels_list[i].users[j].sock != 0){
                    DPRINTF("I am about to send response from send channel\n");
                    send_response(response, channels_list[i].users[j].sock);
                }
            }
        return;
        }
    }

}

// removes user from the channel. Also removes channel from the user.
void remove_user_from_channel(char* channel, client user){
    int i, j;
    for (i = 0; i < MAX_CHANNEL; i++) {
        if(!strcasecmp(channels_list[i].channel_name, channel)){
            for ( j = 0; j < MAX_CLIENTS; j++){
                if(!strcasecmp(channels_list[i].users[j].nick, user.nick)){
                    channels_list[i].users[j] = client_list[MAX_CLIENTS];
                    channels_list[i].client_count--;
                    strcpy(client_list[user.index].channel, client_list[MAX_CLIENTS].channel);
                    delete_channel(i);
                    return;
                }
            }
        }
    }
}


/* Command handlers */
/* MODIFY to take the arguments you specified above! */
// #define CMD_ARGS char *prefix, char **params, int n_params, client client


void cmd_nick(CMD_ARGS)
{
    if (n_params < 1){
        DPRINTF(prefix);
        send_response(response_message_concat(ERR_NEEDMOREPARAMS, "Need More Params", client), client.sock);
        return;
    }

    char response[MAX_MSG_LEN];
    //printf("The nickname given for client %d is %s\n", clientFD, params[0]);
    DPRINTF("The nickname given for client is %s\n" , params[0]);
    char* nick[MAX_MSG_LEN];
    strcpy(nick, params[0]);


    int i;

    for (i = 0; i < MAX_CLIENTS; i++) {

        if((strcasecmp(client.nick, client_list[MAX_CLIENTS].nick) != 0) && (strcasecmp(client.channel, client_list[MAX_CLIENTS].channel) != 0) ){
            trim_string(client.nick);
            trim_string(nick);
            if(strcasecmp(client.nick, nick) == 0){
                DPRINTF("%s has changed it's nick to %s\n", client.nick, nick);

                char* signal;
                strcat(signal, client.nick);
                strcat(signal, " has changed it's nick to");
                strcat(signal, nick);
                strcat(signal, "\n");

                send_channel(signal, client.channel);
                strcpy(client.nick, nick);
                client_list[client.index] = client;
                return;
            }
        }



        if(strcasecmp(client_list[i].nick, nick) == 0){
            DPRINTF("ERR_NICKNAMEINUSE\n");
            send_response(response_message_concat(ERR_NICKNAMEINUSE, "Nickname in use.", client), client.sock);
            return;
        }
    }

    if (strlen(client.user) != 0 && client.registered == 0){
        DPRINTF("Not registered and user name is %s\n", client.user);
        client.registered = 1;
    }
    strcpy(client.nick, nick);
    client_list[client.index] = client;
}

// #define CMD_ARGS char *prefix, char **params, int n_params, client client
void cmd_user(CMD_ARGS)
{

    if (n_params < 4){
        DPRINTF(prefix);
        send_response(response_message_concat(ERR_NEEDMOREPARAMS, "Need More Params", client), client.sock);
        return;
    }
    strcpy(client.user,         params[0]);
    gethostname(client.hostname,MAX_HOSTNAME);
    //strcpy(client.hostname,     params[1]);
    strcpy(client.servername,   params[2]);
    strcpy(client.realname,     params[3]);

    if (client.registered == 1){
        send_response(response_message_concat(ERR_ALREADYREGISTRED, "Already registered.", client), client.sock);
    }


    if (strlen(client.nick) != 0 && client.registered == 0){
        client.registered = 1;
    }

    client_list[client.index] = client;
}

void cmd_quit(CMD_ARGS)
{
    char* quit_message = params[0];
    DPRINTF("Printing the quit message: %s\n", quit_message);
    send_channel(quit_message, client.channel);
    remove_user_from_channel(client.channel, client);
    close(client.sock);
}

void cmd_part(CMD_ARGS)
{
    if (n_params < 1){
        DPRINTF(prefix);
        send_response(response_message_concat(ERR_NEEDMOREPARAMS, "Need More Params", client), client.sock);
        return;
    }

    char response[MAX_MSG_LEN];
    char *channel = params[0];
    int i, j;
    trim_string(channel);
    trim_string(client.channel);

    if(!strcasecmp(channel, client.channel)){
        for (i = 0; i < MAX_CHANNEL; i++) {
            if(!strcasecmp(channels_list[i].channel_name, client.channel)){
                for ( j = 0; j < MAX_CLIENTS; j++){
                    if(!strcasecmp(channels_list[i].users[j].nick, client.nick)){
                        remove_user_from_channel(client.channel, client);
                        return;
                    }
                }
            }
        }
    }else{
        send_response(response_message_concat(ERR_NOSUCHCHANNEL, "No such channel exists.", client), client.sock);
    }

}

void cmd_list(CMD_ARGS)
{
    int i,j;
    DPRINTF("Printing all channels:\n");
    for (i = 0; i < MAX_CHANNEL; i++) {
        if(strcasecmp(channels_list[i].channel_name, "") != 0 ){
            int count = 0;
            for (j = 0; j < MAX_CLIENTS; j++){
                if(strcasecmp(channels_list[i].users[j].nick, "") != 0 ){
                    count = count + 1;
                }
            }
            // make it a sentece and send it back

            trim_string(channels_list[i].channel_name);

            char* response[MAX_MSG_LEN];
            sprintf(response, "%d", count);
            strcat(response,  " members are in channel ");
            strcat(response,  channels_list[i].channel_name);
            strcat(response,  ".\n");
            send_response(response, client.sock);

            DPRINTF("%d members are in channel %s\n",  count, channels_list[i].channel_name);
        }
    }
}

void cmd_join(CMD_ARGS)
{
    if (n_params < 1){
        DPRINTF(prefix);
        send_response(response_message_concat(ERR_NEEDMOREPARAMS, "Need More Params", client), client.sock);
        return;
    }

    char response[MAX_MSG_LEN];
    char* channel = params[0];

    if (channel_count >= MAX_CHANNEL){
        send_response(response_message_concat(ERR_TOOMANYCHANNELS, "Too many channels.", client), client.sock);
        return;
    }

    int already_in_channel;

    if(strlen(client.channel) == 0){
        already_in_channel = 0;
        DPRINTF("already in channel 0\n");
    }else{
        already_in_channel = 1;
        DPRINTF("already in channel 1\n");
    }

    trim_string(channel);
    if( strncmp(channel,"#", 1) == 0){
        DPRINTF("I am in the #.\n");
    }else if( strncmp(channel,"&", 1) == 0) {
        DPRINTF("I am in the &.\n");
    }else{
        send_response(response_message_concat(ERR_NOSUCHCHANNEL, "Channel name not given correctly, please start with either # or &.", client), client.sock);
        return;
    }

    int i,j;

    for (i = 0; i < MAX_CHANNEL; i++) {

        //if channel already exists
        //printf("Printing channel_name: %s\nPrinting channel: %s\nPrinting the result of the compare: %d",channels_list[i].channel_name, channel,
        //strcasecmp(channels_list[i].channel_name, channel) );

        if(strcasecmp(channels_list[i].channel_name, channel) == 0){
            //add the client to the channel
            for(j = 0; j < MAX_CLIENTS; j++){
                if(strcasecmp(channels_list[i].users[j].nick, "") == 0){
                    DPRINTF("Found empty space\n");
                    channels_list[i].users[j] = client;
                    channels_list[i].client_count++;
                    strcpy(client.channel, channel);
                    client_list[client.index] = client;

                    if(already_in_channel){
                        remove_user_from_channel(client.channel, client);
                    }
                    return;
                }
            }
        }
    }

    // create a new channel.
    for(j = 0; j < MAX_CHANNEL; j++){
        if(strlen(channels_list[j].channel_name) == 0){
                DPRINTF("new channel created\n");

                strcpy(channels_list[j].channel_name, channel);
                channels_list[j].users[0] = client;
                channels_list[j].client_count++;

                DPRINTF("%d users exists in active channel: %s\n", channels_list[j].client_count,
                                                                  channels_list[j].channel_name);
                strcpy(client.channel, channel);
                client_list[client.index] = client;

                if(already_in_channel){
                    remove_user_from_channel(client.channel, client);
                }
                channel_count++;
                return;
            }
        }
}


void cmd_privmsg(CMD_ARGS)
{
    if (n_params < 2){
        DPRINTF(prefix);
        send_response(response_message_concat(ERR_NEEDMOREPARAMS, "Need More Params", client), client.sock);
        return;
    }

    int i, j;
    DPRINTF("Sanity check\n");

    char* target = params[0];
    char* message = params[1];

    DPRINTF("Printing the target %s and the message %s\n", target, message);


    for (i = 0; i < MAX_CHANNEL; i++) {
        char* newlineLocation = strstr(channels_list[i].channel_name, "\n");
        if (newlineLocation != NULL){
            strncpy(newlineLocation -1, "\0", 1);
        }
        if(strcasecmp(channels_list[i].channel_name, target) == 0){
            DPRINTF("Found channel and target");
            send_channel(message, channels_list[i].channel_name);
            return;
        }
    }

    int target_fd = clientFD_from_nick(target);
    for (i = 0; i < MAX_CLIENTS; i++) {

        if(!strcasecmp(client_list[i].nick, target)){
            send_response(message, target_fd);
            return;
        }
    }

    send_response(response_message_concat(ERR_NORECIPIENT, "Recepient not found.", client), client.sock);


}

void cmd_who(CMD_ARGS)
{
    int i,j;
    char* target = params[0];
    char* response;

    for (i = 0; i < MAX_CHANNEL; i++) {
        trim_string(channels_list[i].channel_name);
        trim_string(target);
        if(strcasecmp(channels_list[i].channel_name, target) == 0){
            for (j = 0; j < MAX_CLIENTS; j++) {
                if(strcasecmp(channels_list[i].users[j].nick, channels_list[MAX_CHANNEL].users[MAX_CLIENTS].nick)){
                    send_response(strcat(channels_list[i].users[j].nick, ""), client.sock);
                }
            }
            return;
        }
    }
    send_response(response_message_concat(ERR_NOSUCHSERVER, "No such channel exists.", client), client.sock);
}




