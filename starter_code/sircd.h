#ifndef _SIRCD_H_
#define _SIRCD_H_

#include <sys/types.h>
#include <netinet/in.h>

#define MAX_CLIENTS 12
#define MAX_MSG_TOKENS 10
#define MAX_MSG_LEN 512
#define MAX_USERNAME 32
#define MAX_HOSTNAME 512
#define MAX_SERVERNAME 512
#define MAX_REALNAME 512
#define MAX_CHANNAME 512

#define MAX_CHANNEL 20

typedef struct {
    int sock;
    int index;
    struct sockaddr_in cliaddr;
    unsigned inbuf_size;
    int registered;
    char hostname[MAX_HOSTNAME];
    char servername[MAX_SERVERNAME];
    char user[MAX_USERNAME];
    char nick[MAX_USERNAME];
    char realname[MAX_REALNAME];
    char inbuf[MAX_MSG_LEN+1];
    char channel[MAX_CHANNAME];
} client;

typedef struct {
    char channel_name[MAX_CHANNAME];
    client users[MAX_CLIENTS];
    int client_count;
}channels;

extern int channel_count;
extern client client_list[MAX_CLIENTS+1];
extern channels channels_list[MAX_CHANNEL+1];


#endif /* _SIRCD_H_ */
